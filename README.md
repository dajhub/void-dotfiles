### Dotfiles for Void Linux

#### Void xfce Desktop with Everblush Theme
Link to everblush theming: https://github.com/Everblush

![Void - Everblush - xfce](https://i.imgur.com/S9RwEZ2.png)

#### Applications & XFCE scripts

- install script:
  ~~~
  $ sudo ./install.sh
  ~~~

- symlink script:
  ~~~
  $ ./symlink.sh
  ~~~
	
#### Void - Additional Repositories (in install script)
- sudo xbps-install void-repo-nonfree
- sudo xbps-install void-repo-multilib-nonfree

#### Display Manager
To replace lxdm, which the default display manager for void xfce4, with lightdm:
- sudo xbps-install -S lightdm lightdm-gtk3-greeter lightdm-gtk-greeter-settings
- sudo rm /var/service/lxdm
Note: The above command will bounce you out of xfce and into a terminal.  Take a note of the command below so that you can then enter it into the terminal once you've been bounced out:
- sudo ln -s /etc/sv/lightdm /var/service/
- see 'references' below if lightdm-webkit2-greeter is to be installed.

#### Installing a Different Desktop
Rather than install XFCE desktop, the following script will allow other desktops to be installed.  The script is not mine... use at your own risk!!  Requires a [Void base install](https://voidlinux.org/download/) and then:

~~~
$ sudo xbps-install -Su
$ sudo xbps-install -S git
$ git clone https://gitlab.com/dajhub/void-dotfiles.git
$ cd void-dotfiles
$ sudo ./void-install-desktop.sh
~~~

When running through the install script I installed cinnamon desktop as well as openbox and bspwm.  The additional packages which will need to be installed:

~~~
$ sudo xbps-install -S lxappearance-obconf lxappearance rofi dmenu nitrogen setxkbmap xbacklight papirus-icon-theme
~~~

##### Touchpad not working?

Within Openbox and Bspwm touchpad may not work.  My solution was to make a configuration file for touchpad in:

~~~
/etc/X11/xorg.conf.d/30-touchpad.conf
~~~

The content within the file:

~~~
Section "InputClass"
    Identifier "touchpad"
    Driver "libinput"
    MatchIsTouchpad "on"
    Option "Tapping" "on"
EndSection
~~~

#### Virtual Machine - QEMU/KVM
- sudo xbps-install -S virt-manager libvirt qemu
- Start the services that are created by these packages:
	- sudo ln -s /etc/sv/libvirtd /var/service
	- sudo ln -s /etc/sv/virtlockd /var/service
	- sudo ln -s /etc/sv/virtlogd /var/service
- Making sure your user is part of the libvirt group:
	- sudo gpasswd -a "$USER" libvirt

### Miscellaneous
- Keyboard shortcut for rofi 'rofi -show drun'


#### Void Commands
- sudo xbps-install -Su         		### Update Package Lists
- xbps-query -Rs 				### Searching for Packages
- sudo xbps-install -S          		### Installing and Updating Packages
- xbps-query -l | grep vlc      		### Searching Through Installed Packages
- sudo xbps-remove -R           	### Remove a package with all its dependencies

#### My Own Aliases

Located at the end of .bashrc file

```
# Searching for a package:
alias xbs='xbps-query -Rs'

# Installing the package:
alias xbi='sudo xbps-install -S'

# System update:
alias xbu='sudo xbps-install -Su'

#Removing a package with all its dependencies:
alias xbr='sudo xbps-remove -R'

# Removing orphans and package cache:
alias xbo='sudo xbps-remove -Oo'

# Searching Through Installed Packages, e.g. add vlc after alias
alias xbsip='xbps-query -l | grep'
```

#### References
- https://linuxiac.com/void-linux-xbps-package-manager/
- theme for lightdm-webkit2-greeter -- https://github.com/thegamerhat/lightdm-glorious-webkit2 or https://github.com/dimaglushkov/lightdm-webkit2-theme-minimal 

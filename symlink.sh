#!/bin/bash

mydotfiles=$HOME/void-dotfiles

ln -s $mydotfiles/.config/rofi  ~/.config
ln -s $mydotfiles/.config/kitty  ~/.config
ln -s $mydotfiles/.config/geany  ~/.config
ln -s $mydotfiles/.config/neofetch  ~/.config

ln -s $mydotfiles/.config/dunst  ~/.config
ln -s $mydotfiles/.config/obmenu-generator  ~/.config
ln -s $mydotfiles/.config/openbox  ~/.config
ln -s $mydotfiles/.config/sxhkd  ~/.config
ln -s $mydotfiles/.config/tint2  ~/.config

ln -s $mydotfiles/.config/picom.conf  ~/.config
ln -s $mydotfiles/.config/.config/gtk.css ~/.config/gtk-3.0
ln -s $mydotfiles/Wallpapers  ~/Pictures
ln -s $mydotfiles/.themes  ~/.themes
ln -s $mydotfiles/.fonts  ~/.fonts
ln -s $mydotfiles/.icons  ~/.icons
ln -s $mydotfiles/.local/bin  ~/.local
